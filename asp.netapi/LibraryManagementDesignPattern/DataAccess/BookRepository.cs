using LibraryManagementDesignPattern.Context;
using LibraryManagementDesignPattern.DTO;
using LibraryManagementDesignPattern.Model;

namespace LibraryManagementDesignPattern.DataAccess;

public class BookRepository : IBookRepository
{
    private readonly AppDbContext _dbContext;

    public BookRepository(AppDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public List<Book> GetAllBooks()
    {
        return _dbContext.Books.ToList();
    }

    public Book GetBookById(int id)
    {
        return _dbContext.Books.FirstOrDefault(entry => entry.Id == id);
    }

    public void CreateBook(BookDTO book)
    {
        var newBook = new Book()
        {
            Title = book.Title,
            Author = book.Author,
            ISBN = book.ISBN,
            PublicationYear = book.PublicationYear,
            IsActive = true
        };
        _dbContext.Books.Add(newBook);
    }

    public void UpdateBook(int id, BookDTO updateBook)
    {
        var book = _dbContext.Books.FirstOrDefault(entry => entry.Id == id);
        if (book is null)
        {
            throw new Exception($"Not found book id: {id}");
        }

        book.Title = updateBook.Title;
        book.Author = updateBook.Author;
        book.ISBN = updateBook.ISBN;
        book.PublicationYear = updateBook.PublicationYear;
    }

    public void RemoveBook(int id)
    {
        var book = _dbContext.Books.FirstOrDefault(entry => entry.Id == id);
        if (book is null)
        {
            throw new Exception($"Not found book id: {id}");
        }

        _dbContext.Books.Remove(book);
    }

    public void ChangeStatus(int id)
    {
        var book = FindById(id);
        book.IsActive = !book.IsActive;
    }

    private Book FindById(int id)
    {
        var book = _dbContext.Books.FirstOrDefault(entry => entry.Id == id);
        if (book is null)
        {
            throw new Exception($"Not found book id: {id}");
        }

        return book;
    }
}