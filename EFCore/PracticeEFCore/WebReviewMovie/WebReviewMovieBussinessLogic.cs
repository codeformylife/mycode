using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace WebReviewMovie;

public class WebReviewMovieBussinessLogic
{
    private AppDbContext context = new AppDbContext();
    private int pageNumber = 1;
    private int pageSize = 5;


    public void LoadData<T>(string json_file)
    {
        var entityName = typeof(T).Name + "s";
        var streamReader = new StreamReader(json_file);
        var jsonTextReader = new JsonTextReader(streamReader);
        var json = new JsonSerializer();
        var obj = json.Deserialize<List<T>>(jsonTextReader);
        if (obj is not null)
        {
            if (entityName.Equals("Movies"))
            {
                context.Movies.AddRange(obj as IEnumerable<Movie>);
                context.SaveChanges();
            }
            else
            {
                context.Reviews.AddRange(obj as IEnumerable<Review>);
                context.SaveChanges();
            }
        }
        else
        {
            throw new Exception("Json file is null");
        }
    }

    public void NextPage()
    {
        if (pageNumber <= MaxPage())
        {
            pageNumber++;
        }

        ShowPage();
    }

    public void PreviousPage()
    {
        if (pageNumber > 1)
        {
            pageNumber--;
        }

        ShowPage();
    }

    public void ShowPage()
    {
        var movies = context.Movies
            .OrderBy(entry => entry.Id)
            .Skip((pageNumber - 1) * pageSize)
            .Take(pageSize)
            .ToList();
        Console.WriteLine($"Page: {pageNumber}");

        foreach (var movie in movies)
        {
            Console.WriteLine("---------------------------------------------------------------------------------------------------------------------------------");
            Console.WriteLine(
                $"Id: {movie.Id} | Name: {movie.Name} | Director: {movie.Director} | Genre: {movie.Genre} |");
            Console.WriteLine("---------------------------------------------------------------------------------------------------------------------------------");

        }
    }

    private double MaxPage()
    {
        var totalRecord = context.Movies.Count();

        return Math.Ceiling((double)(totalRecord / pageSize));
    }


    private void ShowMovieDetail(Movie movie)
    {
        Console.WriteLine("---------------------------------------------------------------------------------------------------------------------------------");
        Console.WriteLine(
            $"Id: {movie.Id} | Name: {movie.Name} | Create At: {movie.CreateAt:MM/dd/yyyy} | Director: {movie.Director} | Country: {movie.Country} | Description: {movie.Description} | Genre: {movie.Genre} | IsActive: {movie.IsActive}");
        Console.WriteLine("---------------------------------------------------------------------------------------------------------------------------------");

    }

    public Movie FindMovieById(int id)
    {
        var movie = context.Movies.FirstOrDefault(entry => entry.Id == id);
        if (movie is null)
        {
            throw new Exception("Movie is not found");
        }

        return movie;
    }

    public void UpdateMovie(Movie oldMovie, Movie updateMovie)
    {
        if (updateMovie.Name.Equals("") is true)
        {
            oldMovie.Name = oldMovie.Name;
        }

        oldMovie.Name = updateMovie.Name;

        if (updateMovie.CreateAt.ToString("MM/dd/yyyy").Equals("") is true)
        {
            oldMovie.CreateAt = oldMovie.CreateAt;
        }

        oldMovie.CreateAt = updateMovie.CreateAt;
        if (updateMovie.Director.Equals("") is true)
        {
            oldMovie.Director = oldMovie.Director;
        }

        oldMovie.Director = updateMovie.Director;

        if (updateMovie.Country.Equals("") is true)
        {
            oldMovie.Country = oldMovie.Country;
        }

        oldMovie.Country = updateMovie.Country;
        if (updateMovie.Description.Equals("") is true)
        {
            oldMovie.Description = oldMovie.Description;
        }

        oldMovie.Description = updateMovie.Description;
        if (updateMovie.Genre.Equals("") is true)
        {
            oldMovie.Genre = oldMovie.Genre;
        }

        oldMovie.Genre = updateMovie.Genre;

        context.SaveChanges();
    }

    public void UpdateActive(Movie movie)
    {
        movie.IsActive = movie.IsActive is false;
        context.SaveChanges();
    }

    public void RemoveMovie(Movie movie)
    {
        context.Movies.Remove(movie);
        context.SaveChanges();
    }

    public void AddNewReview(Review review)
    {
        context.Reviews.Add(review);
        context.SaveChanges();
    }

    public void ShowReviewOfMovie(Movie movie)
    {
        Helper.Title("Movie");
        ShowMovieDetail(movie);
        Helper.Title($"Review of {movie.Name}");
        var showReviewOfMovie = context.Reviews.Where(entry => entry.MovieId == movie.Id).ToList();
        foreach (var review in showReviewOfMovie)
        {
            Console.WriteLine("---------------------------------------------------------------------------------------------------------------------------------");
            Console.WriteLine($"Title: {review.Title} | Score: {review.Score} | Content: {review.Content}");
            Console.WriteLine("---------------------------------------------------------------------------------------------------------------------------------");

        }
    }

    public void AddNewMovie(Movie movie)
    {
        context.Movies.Add(movie);
        context.SaveChanges();
    }

    public void FilterMovieByName(string name)
    {
        var movies = context.Movies.Where(entry => entry.Name.Contains(name)).ToList();
        foreach (var movie in movies)
        {
            ShowMovieDetail(movie);
        }
    }

    public void FilterMovieByYearOfRelease(int yearOfRelease)
    {
        var movies = context.Movies.Where(entry => entry.CreateAt.Year.Equals(yearOfRelease)).ToList();
        foreach (var movie in movies)
        {
            ShowMovieDetail(movie);
        }
    }

    public void FilterMovieByDirector(string director)
    {
        var movies = context.Movies.Where(entry => entry.Name.Contains(director)).ToList();
        foreach (var movie in movies)
        {
            ShowMovieDetail(movie);
        }
    }


    public void FilterMovieByCountry(string country)
    {
        var movies = context.Movies.Where(entry => entry.Name.Contains(country)).ToList();
        foreach (var movie in movies)
        {
            ShowMovieDetail(movie);
        }
    }
}