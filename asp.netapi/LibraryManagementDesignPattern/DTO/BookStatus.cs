namespace LibraryManagementDesignPattern.DTO;

public class BookStatus
{
    public bool IsActive { get; set; }
}