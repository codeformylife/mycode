using Microsoft.EntityFrameworkCore;

namespace StudentManagement_EFCore;

public class AppDbContext: DbContext
{
    public DbSet<Student> Students { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var connectionStrings =
            "Data Source=localhost;Database=Student_Management;User Id= sa;Password=reallyStrongPwd123;Trusted_Connection=False;TrustServerCertificate=True";
        optionsBuilder.UseSqlServer(connectionStrings);
    }
    
}