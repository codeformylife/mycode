using Microsoft.EntityFrameworkCore;

namespace LINQToEntities_EFCore;

public class AppDbContext : DbContext
{
    public DbSet<Product> Products { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        const string connectionString = "Data Source= localhost;Database=LINQToEntity; User Id = sa; Password= reallyStrongPwd123;Trusted_Connection=False;TrustServerCertificate=True";
        optionsBuilder.UseSqlServer(connectionString);
    }
}