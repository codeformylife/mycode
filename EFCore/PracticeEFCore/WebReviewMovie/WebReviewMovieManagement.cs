using System.Xml;

namespace WebReviewMovie;

public class WebReviewMovieManagement
{
    private WebReviewMovieBussinessLogic control = new WebReviewMovieBussinessLogic();

    public void OptionViewListMovie()
    {
        char option;
        do
        {
            control.ShowPage();
            Menu.PageMenu();
            option = Helper.InputActionPage();
            switch (option)
            {
                case 'N':
                    control.NextPage();
                    Helper.ClearConsole();
                    break;
                case 'P':
                    control.PreviousPage();
                    Helper.ClearConsole();
                    break;
                case 'A':
                    var id = Helper.InputInteger("Enter movie's id to more option: ");
                    OptionViewListMovie(id);
                    Helper.ClearConsole();
                    break;
                case 'B':
                    break;
                default:
                    break;
            }
            
        } while (option != 'B');
    }

    private void OptionViewListMovie(int id)
    {
        int option = 0;
        do
        {
            ViewAllReview(id);
            Menu.OptionChangeMovieAndReviewMenu();
            option = Helper.InputInteger("Enter your option: ");
            switch (option)
            {
                case 1:
                    UpdateInformationMovie(id);
                    Helper.ClearConsole();
                    break;
                case 2:
                    RemoveMovie(id);
                    Helper.ClearConsole();
                    break;
                case 3:
                    ChangeActiveOfMovie();
                    break;
                case 4: 
                    AddNewReviewOfMovie(id);
                    Helper.ClearConsole();
                    break;
                case 5:
                    break;
            }
        } while (option != 5);
    }

    public void OptionSearchMovie()
    {
        var option = 0;
        do
        {
            Menu.OptionSearchMovie();
            option = Helper.InputInteger("Enter your option: ");
            Helper.ClearConsole();
            switch (option)
            {
                case 1:
                    SearchMovieByName();
                    break;
                case 2:
                    SearchMovieByCountry();
                    break;
                case 3:
                    SearchMovieByDirector();
                    break;
                case 4:
                    SearchMovieByYearOfRelease();
                    break;
                case 5:
                    break;
                default:
                    break;
            }
        } while (option != 5);
    }
    
    private void UpdateInformationMovie(int movieId)
    {
        var existMovie = control.FindMovieById(movieId);
        if (existMovie is null)
        {
            throw new Exception("Movie is not found");
        }

        var updateMovie = new Movie
        {
            Name = Helper.InputString("Enter movie's name: "),
            CreateAt = Helper.InputDateTime("Enter create at time:"),
            Director = Helper.InputString("Enter movie's director"),
            Country = Helper.InputString("Enter movie's country: "),
            Description = Helper.InputString("Enter movie's description: "),
            Genre = Helper.InputString("Enter movie's genre: ")
        };
        control.UpdateMovie(existMovie, updateMovie);
    }

    private void RemoveMovie(int movieId)
    {
        var removeMovie = control.FindMovieById(movieId);
        if (removeMovie is null)
        {
            throw new Exception("Movie is not found");
        }

        var answer = Helper.InputQuestion("Do you wanna to remove this movie");
        if (answer is true)
        {
            control.RemoveMovie(removeMovie);
            Console.WriteLine("Remove movie is approve");
        }
        else
        {
            Console.WriteLine("Remove movie is reject");
        }
    }

    private void AddNewReviewOfMovie(int movieId)
    {
        var existMovie = control.FindMovieById(movieId);
        if (existMovie is null)
        {
            throw new Exception("Movie is not found");
        }

        var newReview = new Review
        {
            Title = Helper.InputString("Enter review's title: "),
            Score = Helper.InputScore("Enter review's score: "),
            Content = Helper.InputString("Enter review's content: "),
            MovieId = movieId
        };
        control.AddNewReview(newReview);
    }

    private void ViewAllReview(int movieId)
    {
        var existMovie = control.FindMovieById(movieId);
        if (existMovie is null)
        {
            throw new Exception("Movie is not found");
        }

        control.ShowReviewOfMovie(existMovie);
    }

    public void AddNewMovie()
    {
        var newMovie = new Movie
        {
            Name = Helper.InputString("Enter movie's name: "),
            CreateAt = Helper.InputDateTime("Enter create at time:"),
            Director = Helper.InputString("Enter movie's director"),
            Country = Helper.InputString("Enter movie's country: "),
            Description = Helper.InputString("Enter movie's description: "),
            Genre = Helper.InputString("Enter movie's genre: "),
            IsActive = false
        };
        control.AddNewMovie(newMovie);
    }

    private void ChangeActiveOfMovie()
    {
        var id = Helper.InputInteger("Enter movie's id to change active");
        var activeMovie = control.FindMovieById(id);
        control.UpdateActive(activeMovie);
    }

    public void SearchMovieByName()
    {
        var nameMovie = Helper.InputString("Enter movie's name to search");
        control.FilterMovieByName(nameMovie);
    }

    public void SearchMovieByCountry()
    {
        var countryMovie = Helper.InputString("Enter movie's country to search");
        control.FilterMovieByCountry(countryMovie);
    }

    public void SearchMovieByYearOfRelease()
    {
        var yearOfReleaseMovie = Helper.InputInteger("Enter movie's year of release to search");
        control.FilterMovieByYearOfRelease(yearOfReleaseMovie);
    }

    public void SearchMovieByDirector()
    {
        var directorMovie = Helper.InputString("Enter movie's name to search");
        control.FilterMovieByDirector(directorMovie);
    }
}