using LibraryManagementDesignPattern.Model;
using Microsoft.EntityFrameworkCore;

namespace LibraryManagementDesignPattern.Context;

public class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
    {
    }

    public DbSet<Book> Books { get; set; }
}