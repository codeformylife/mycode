using LibraryManagementDesignPattern.DTO;
using LibraryManagementDesignPattern.Model;
using LibraryManagementDesignPattern.UoW;
using Microsoft.AspNetCore.Mvc;

namespace LibraryManagementDesignPattern.Controller;

[ApiController]
[Route("api/[controller]/[action]")]
public class BooksController : ControllerBase
{
    private readonly IUnitOfWork _unitOfWork;

    public BooksController(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    [HttpGet]
    public List<Book> Books()
    {
        return _unitOfWork.BookRepository.GetAllBooks();
    }

    [HttpGet("{id}")]
    public Book Book(int id)
    {
        return _unitOfWork.BookRepository.GetBookById(id);
    }

    [HttpPost]
    public IActionResult CreateBook(BookDTO book)
    {
        _unitOfWork.BookRepository.CreateBook(book);
        var rowEffect = _unitOfWork.SaveChange();
        if (rowEffect == 1)
        {
            return Ok("CreateBook Success");
        }

        return BadRequest("CreateBook Failed");
    }

    [HttpPut("{id}")]
    public IActionResult UpdateBook([FromRoute] int id, [FromBody] BookDTO updateBook)
    {
        _unitOfWork.BookRepository.UpdateBook(id, updateBook);
        _unitOfWork.SaveChange();
        return Ok("Update Book success");
    }

    [HttpDelete("{id}")]
    public IActionResult RemoveBook([FromRoute] int id)
    {
        _unitOfWork.BookRepository.RemoveBook(id);
      var rowEffect=  _unitOfWork.SaveChange();
      if (rowEffect == 1)
      {
          return Ok("Remove book id: {id} success");
      }

      return BadRequest("Remove book id: {id} failed");
    }
}