﻿// khởi tạo đối tượng context đại diện cho 1 database

using LINQToEntities_EFCore;
using Newtonsoft.Json;

var context = new AppDbContext();

// tạo danh sách Product sẽ thêm vào database
var s = new StreamReader(
    "/Users/macos/CodeForMyLife/EFCore/LINQToEntities_EFCore/LINQToEntities_EFCore/MOCK_DATA.json");
var jr = new JsonTextReader(s);
var js = new JsonSerializer();
var obj = js.Deserialize<List<Product>>(jr);
// // đánh dấu là thêm vào database
// context.Products.AddRange(obj);
//
// // lưu mọi sự thay đổi
// context.SaveChanges();
// var number = context.Products.Count();
// Console.WriteLine($"number of record is: {number}");

//Lấy danh sách tất cả các sản phẩm.
// var allProduct = context.Products.Take(10).ToList();
// foreach (var product in allProduct)
// {
//     Console.WriteLine($"Name: {product.Name}");
// }

//Lấy danh sách tên của tất cả các sản phẩm.
//
// var nameProduct = context.Products.Select(entry => entry.Name).Take(10).ToList();
// foreach (var name in nameProduct)
// {
//     Console.WriteLine($"Name: {name}");
// }

//Lấy danh sách các sản phẩm có giá lớn hơn 50 đồng.
// var priceLargest50Cent = context.Products.Where(entry => entry.Price >= 50).Take(10).ToList();
// foreach (var product in priceLargest50Cent)
// {
//     Console.WriteLine($"Name : {product.Name} | Price: {product.Price}");
// }
//Lấy danh sách các sản phẩm có số lượng tồn kho ít hơn 10.
// var stockQuantity = context.Products.Where(entry => entry.StockQuantity <= 10).Take(10).ToList();
// foreach (var product in stockQuantity)
// {
//     Console.WriteLine($"Name: {product.Name} | StockQuantity: {product.StockQuantity}");
// }

//Lấy danh sách tên của các sản phẩm có giá từ 100 đến 500 đồng.
// var products = context.Products.Where(entry => entry.Price >= 100 && entry.Price <= 500).Take(10).ToList();
// foreach (var product in products)
// {
//     Console.WriteLine($"Name: {product.Name} | Price: {product.Price}");
// }

// //Lấy danh sách các sản phẩm được tạo ra trong vòng 7 ngày gần đây.
// var day = DateTime.Now.AddDays(-7);
//
// var products = context.Products.Where(entry => entry.CreatedAt >= day).
//         Take(1000).ToList();
// foreach (var product in products)
// {
//     Console.WriteLine($"Name: {product.Name}");
// }


//Lấy danh sách các sản phẩm không còn tồn tại (không hoạt động).
// var products = context.Products.Where(entry => entry.IsActive == false).Take(10).ToList();
// foreach (var product in products)
// {
//     Console.WriteLine($"Name: {product.Name}| IsActive: {product.IsActive}");
// }

// //Lấy tổng số lượng tồn kho của tất cả sản phẩm.
// var stockQuantity = context.Products.Sum(entry => entry.StockQuantity);
// Console.WriteLine($"Sum stock: {stockQuantity}");

// //Lấy tên của sản phẩm và ngày tạo của sản phẩm có giá lớn hơn 200 đồng.
// var products = context.Products.Where(entry => entry.Price >= 200)
//     .Select(entry => new { Name = entry.Name, CreateTime = entry.CreatedAt }).ToList();
// foreach (var product in products)
// {
//     Console.WriteLine($"name: {product.Name} | CreateTime: {product.CreateTime}");
// }

//Lấy danh sách các sản phẩm được sắp xếp theo giá giảm dần.
// var products = context.Products.OrderByDescending(entry => entry.Price).Take(10).ToList();
// foreach (var product in products)
// {
//     Console.WriteLine($"Name: {product.Name} | Price: {product.Price}");
// }

//Order By:

//Lấy danh sách các sản phẩm được sắp xếp theo tên sản phẩm tăng dần.
// var products = context.Products.OrderBy(entry => entry.Name).Take(10).ToList();
// foreach (var product in products)
// {
//     Console.WriteLine($"{product.Name}");
// }

//Lấy danh sách các sản phẩm được sắp xếp theo giá giảm dần.
//Lấy danh sách các sản phẩm được sắp xếp theo ngày tạo mới nhất đến cũ nhất.
// var product = context.Products.OrderByDescending(entry => entry.CreatedAt).Take(10).ToList();
// foreach (var pro in product)
// {
//     Console.WriteLine($"{pro.Name} | {pro.CreatedAt}");
// }


//Group By:

//Lấy danh sách các sản phẩm được nhóm theo số lượng tồn kho. (Mỗi nhóm hiển thị số lượng và tên các sản phẩm trong nhóm đó)
// var products = context.Products.GroupBy(entry => entry.StockQuantity);
// foreach (var product in products)
// {
//     foreach (var item in product)
//     {
//         Console.WriteLine($"Name: {item.Name} | StockQuantity: {item.StockQuantity}");   
//     }
// }
//Lấy danh sách các sản phẩm được nhóm theo khoảng giá (0-100, 101-200, v.v.). (Mỗi nhóm hiển thị khoảng giá và số lượng sản phẩm trong nhóm đó)

//First, FirstOrDefault, Last, LastOrDefault, Single, SingleOrDefault:

// //Lấy thông tin chi tiết của sản phẩm đầu tiên trong danh sách.
// var firstProduct = context.Products.First();
// Console.WriteLine(firstProduct.Name);
// //Lấy thông tin chi tiết của sản phẩm có giá cuối cùng.
// var lastProduct = context.Products.OrderBy(entry => entry).Last();
// Console.WriteLine(lastProduct.Name);
//Lấy thông tin chi tiết của sản phẩm có tên là "iPhone" (nếu có).
var iPhone = context.Products.FirstOrDefault(entry => entry.Name.Equals("iPhone"));
Console.WriteLine(iPhone.Name);
//Lấy thông tin chi tiết của sản phẩm có ID là 5 (nếu có).
var IDEqual5 = context.Products.FirstOrDefault(entry => entry.Id == 5);
Console.WriteLine(IDEqual5.Id);
//Lấy thông tin chi tiết của sản phẩm có giá là 100 (nếu có).


//Paging:

//Lấy danh sách 10 sản phẩm đầu tiên trong danh sách.
//Lấy danh sách 20 sản phẩm từ vị trí thứ 21 trong danh sách.