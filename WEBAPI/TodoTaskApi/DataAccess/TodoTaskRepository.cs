namespace TodoTaskApi.DataAccess;

public class TodoTaskManagement
{
    private List<Task> _tasks;

    public TodoTaskManagement()
    {
        _tasks = new List<Task>();
    }

    public List<Task> GetAllTasks()
    {
        return _tasks;
    }

    public Task GetTaskById(int taskId)
    {
        var task = _tasks.FirstOrDefault(entry => entry.Id == taskId);
        if (task is null)
        {
            throw new Exception("Task doesn't exits");
        }

        return task;

    }
    public List<Task> AddNewTask(Task task)
    {
        _tasks.Add(task);
        return _tasks;
    }
    
    
    
}