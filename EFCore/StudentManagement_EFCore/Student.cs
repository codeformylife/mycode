using System.ComponentModel.DataAnnotations;

namespace StudentManagement_EFCore;

public class Student
{
    public Student(string name, int age, string gender, string major)
    {
        Name = name;
        Age = age;
        Gender = gender;
        Major = major;
    }

    [Key] public int Id { get; set; }
    [Required] public string Name { get; set; }
    [Required] public int Age { get; set; }
    [Required] public string Gender { get; set; }
    [Required] public string Major { get; set; }
}