namespace WebReviewMovie;

public static class Helper
{
    public static int InputInteger(string message)
    {
        try
        {
            Console.WriteLine(message);
            int input = int.Parse(Console.ReadLine()!);
            return input;
        }
        catch (Exception e)
        {
            throw new Exception($"Error: {e.Message}");
        }
    }


    public static DateTime InputDateTime(string message)
    {
        Console.WriteLine($"{message}");
        DateTime time = DateTime.Parse(Console.ReadLine()!);
        if (time.Year < 1892 || time.Year > DateTime.Now.Year)
        {
            throw new Exception($"Error: Time is invalid");
        }

        return time;
    }

    public static string InputString(string message)
    {
        try
        {
            Console.WriteLine(message);
            var str = Console.ReadLine()!;
            return str;
        }
        catch (Exception e)
        {
            throw new Exception($"Error: {e.Message}");
        }
    }

    public static bool InputQuestion(string message)
    {
        Console.WriteLine(message);
        var answer = Console.ReadLine()!.ToUpper();
        if (answer.Equals("Y"))
        {
            return true;
        }
        else if (answer.Equals("N"))
        {
            return false;
        }
        else
        {
            throw new Exception("Option is invalid");
        }
    }

    public static double InputScore(string message)
    {
        Console.WriteLine(message);
        double db = double.Parse(Console.ReadLine()!);
        if (db is < 0 or > 10.0)
        {
            throw new Exception("Error score must in 0 -> 10");
        }

        return db;
    }

    public static char InputActionPage()
    {
        try
        {
            char letter = char.Parse(Console.ReadLine()!.ToUpper());
            return letter;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
    }

    // public static void FeatureTitle(string message)
    // {
    //     Console.WriteLine("*******************************************");
    //     Console.WriteLine($"{message}");
    //     Console.WriteLine("*******************************************");
    // }

    public static void Title(string message)
    {
        Console.WriteLine("******************");
        Console.WriteLine($"*  {message}  *");
        Console.WriteLine("******************");
    }

    public static void ClearConsole()
    {
        Console.Clear();
    }
}