using Microsoft.EntityFrameworkCore;

namespace StudentManagement_EFCore;

public class StudentManagement
{
    private AppDbContext context;

    public StudentManagement()
    {
        context = new AppDbContext();
    }

    public void AddNewStudent(Student student)
    {
        context.Students.Add(student);
        context.SaveChanges();
    }

    public void AddMultipleStudent(List<Student> students)
    {
        context.Students.AddRange(students);
        context.SaveChanges();
    }

    public void ViewStudent()
    {
        var students = context.Students.ToList();
        if (students is null)
        {
            throw new Exception("Database haven't any student");
        }

        foreach (var student in students)
        {
            ViewFormatOfStudent(student);
        }
    }

    public void UpdateInformationStudent(Student student, Student updateStudent)
    {
        if (!updateStudent.Name.Equals(""))
        {
            student.Name = updateStudent.Name;
        }

        student.Name = student.Name;
        if (updateStudent.Age != 0)
        {
            student.Age = updateStudent.Age;
        }

        student.Age = student.Age;

        if (!updateStudent.Major.Equals(""))
        {
            student.Gender = updateStudent.Gender;
        }

        student.Gender = student.Gender;

        if (!updateStudent.Major.Equals(""))
        {
            student.Major = updateStudent.Major;
        }

        student.Major = student.Major;

        context.SaveChanges();
    }

    public void RemoveStudent(Student student)
    {
        var removeStudent = context.Students.Remove(student);
        context.SaveChanges();
    }

    public void ViewStudentByName(List<Student> students)
    {
        foreach (var student in students)
        {
            ViewFormatOfStudent(student);
        }
    }

    public int ViewTotalOfAmountStudent()
    {
        var totalStudent = context.Students.ToList().Count;
        return totalStudent;
    }

    public double ViewPercentOfMaleStudent()
    {
        double countStudents = context.Students.Count();
        double countMaleStudents = context.Students.Count(entry => entry.Gender.Equals("Male"));
        return Math.Round(countMaleStudents / countStudents * 100.0, 2);
    }

    public double ViewPercentOfFeMaleStudent()
    {
        double countStudents = context.Students.Count();
        double countFemaleStudents = context.Students.Count(entry => entry.Gender.Equals("Female"));
        return Math.Round(countFemaleStudents / countStudents * 100.0, 2);
    }

    public List<Student> FindStudentByName(string _studentName)
    {
        var students = context.Students.Where(entry => entry.Name.Contains(_studentName)).ToList();
        if (students is null)
        {
            throw new Exception($"Student with name: {_studentName} not found");
        }

        return students;
    }

    public Student FindStudentById(int _studentId)
    {
        var student = context.Students.FirstOrDefault(entry => entry.Id == _studentId);
        if (student is null)
        {
            throw new Exception($"Student with Id: {_studentId} not found");
        }

        return student;
    }

    private void ViewFormatOfStudent(Student student)
    {
        Console.WriteLine($"Name: {student.Name}| Age: {student.Age}| Gender:{student.Gender}| Major: {student.Major}");
    }
}