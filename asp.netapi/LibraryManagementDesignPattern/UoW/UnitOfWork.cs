using LibraryManagementDesignPattern.Context;
using LibraryManagementDesignPattern.DataAccess;

namespace LibraryManagementDesignPattern.UoW;

public class UnitOfWork : IUnitOfWork
{
    private readonly AppDbContext _dbContext;

    public UnitOfWork(AppDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public int SaveChange() => _dbContext.SaveChanges();

    public IBookRepository BookRepository => new BookRepository(_dbContext);
}