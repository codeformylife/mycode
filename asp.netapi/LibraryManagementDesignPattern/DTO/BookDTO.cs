namespace LibraryManagementDesignPattern.DTO;

public class BookDTO
{
    public string Title { get; set; }
    public string Author { get; set; }
    public int ISBN { get; set; }
    public string PublicationYear { get; set; }
    
}