﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebReviewMovie.Migrations
{
    /// <inheritdoc />
    public partial class ChangeProperties : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "YearOfRelease",
                table: "Movies",
                newName: "CreateAt");

            migrationBuilder.RenameColumn(
                name: "Status",
                table: "Movies",
                newName: "IsActive");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsActive",
                table: "Movies",
                newName: "Status");

            migrationBuilder.RenameColumn(
                name: "CreateAt",
                table: "Movies",
                newName: "YearOfRelease");
        }
    }
}
