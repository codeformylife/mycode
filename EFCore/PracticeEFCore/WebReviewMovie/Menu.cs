namespace WebReviewMovie;

public static class Menu
{
    public static void MainMenu()
    {
        Console.WriteLine("******************************************");
        Console.WriteLine("*        1. View list movie              *");
        Console.WriteLine("*        2. Add new movie                *");
        Console.WriteLine("*        3. Filter movie                 *");
        Console.WriteLine("*        4. Exit                         *");
        Console.WriteLine("******************************************");
    }

    public static void PageMenu()
    {
        Console.WriteLine("******************************************");
        Console.WriteLine("*        1. Press 'N' to next page       *");
        Console.WriteLine("*        2. Press 'P' to previous page   *");
        Console.WriteLine("*        3. Press 'A' to choice movie    *");
        Console.WriteLine("*        3. Press 'B' to back menu       *");
        Console.WriteLine("******************************************");
    }

    public static void OptionChangeMovieAndReviewMenu()
    {
        Console.WriteLine("******************************************");
        Console.WriteLine("* 1. Update movie                        *");
        Console.WriteLine("* 2. Remove movie                        *");
        Console.WriteLine("* 3. Update movie is active              *");
        Console.WriteLine("* 4. Add new review                      *");
        Console.WriteLine("* 5. Back to Menu                        *");
        Console.WriteLine("******************************************");
        
    }

    public static void OptionSearchMovie()
    {
        Console.WriteLine("******************************************");
        Console.WriteLine("* 1. Search movie by name                *");
        Console.WriteLine("* 2. Search movie by country             *");
        Console.WriteLine("* 3. Search movie by director            *");
        Console.WriteLine("* 4. Search movie by genre               *");
        Console.WriteLine("* 5. Back to Menu                        *");
        Console.WriteLine("******************************************");
    }
}