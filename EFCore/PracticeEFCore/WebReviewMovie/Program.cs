﻿using WebReviewMovie;

var management = new WebReviewMovieManagement();
int option = 0;
do
{
    try
    {
        Menu.MainMenu();
        option = Helper.InputInteger("Enter your option: ");
        Helper.ClearConsole();
        switch (option)
        {
            case 1:
                management.OptionViewListMovie();
                break;
            case 2:
                management.AddNewMovie();
                break;
            case 3:
                management.OptionSearchMovie();
                break;
            case 4:
                Console.WriteLine("Thank for using service");
                break;
            default:
                break;
        }

    }
    catch (Exception e)
    {
        Console.WriteLine(e.Message);
        
    }
}while(option != 4);