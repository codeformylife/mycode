using LibraryManagementDesignPattern.DTO;
using LibraryManagementDesignPattern.Model;

namespace LibraryManagementDesignPattern.DataAccess;

public interface IBookRepository
{
    public List<Book> GetAllBooks();
    public Book GetBookById(int id);
    public void CreateBook(BookDTO book);
    public void UpdateBook(int id, BookDTO updateBook);
    public void RemoveBook(int id);
    public void ChangeStatus(int id);

}