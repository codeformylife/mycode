namespace WebReviewMovie;

public class Movie
{
    public int Id { get; set; }
    public string Name { get; set; }
    public DateTime CreateAt { get; set; }
    public string Director { get; set; }
    public string Country { get; set; }
    public string Description { get; set; }
    public string Genre { get; set; }
    public bool IsActive { get; set; }
    public ICollection<Review> Reviews { get; set; }
    
}