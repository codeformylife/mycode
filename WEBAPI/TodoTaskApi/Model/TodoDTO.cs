namespace TodoTaskApi.Model;

public class TodoDTO
{
    public string Title { get; set; }
    public string Content { get; set; }
    public DateTime CreateAt { get; set; }
    public bool IsActive { get; set; }
}