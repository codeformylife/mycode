using LibraryManagementDesignPattern.DataAccess;

namespace LibraryManagementDesignPattern.UoW;

public interface IUnitOfWork
{
    public int SaveChange();
    IBookRepository BookRepository { get; }
}