using Microsoft.EntityFrameworkCore;

namespace WebReviewMovie;

public class AppDbContext : DbContext
{
    public DbSet<Movie> Movies { get; set; }
    public DbSet<Review> Reviews { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var connectionString =
            "Data Source = localhost; Database = WebReviewMovie; User Id = sa; Password= reallyStrongPwd123; Trusted_Connection= False;TrustServerCertificate=True";
        optionsBuilder.UseSqlServer(connectionString);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Movie>()
            .HasMany(r => r.Reviews)
            .WithOne(m => m.Movie)
            .HasForeignKey(m => m.MovieId);
    }
    
}