using LibraryManagementDesignPattern.Context;
using LibraryManagementDesignPattern.UoW;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllers();
var connect =
    "Data Source=localhost;Database=LibraryManagementApi;User Id= sa;Password=reallyStrongPwd123;Trusted_Connection=False;TrustServerCertificate=True";
builder.Services.AddDbContext<AppDbContext>(entry => entry.UseSqlServer(connect));
builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
var app = builder.Build();

app.MapControllers();

app.Run();