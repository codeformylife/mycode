namespace LibraryManagementDesignPattern.Model;

public class Book
{
    public int Id { get; set; }
    public string Title { get; set; }
    public string Author { get; set; }
    public int ISBN { get; set; }
    public string PublicationYear { get; set; }
    public bool IsActive { get; set; }
}