﻿using StudentManagement_EFCore;

var mamagement = new StudentManagement();
const int AMOUNT_OPTION = 9;
var choice = 0;
do
{
    try
    {
        MainMenu();
        choice = GetChoice();
        Console.Clear();
        switch (choice)
        {
            case 1:
                AddNewStudent();
                BackToMenu();
                break;
            case 2:
                ViewAllStudent();
                BackToMenu();
                break;
            case 3:
                UpdateInformationStudent();
                BackToMenu();
                break;
            case 4:
                RemoveStudent();
                BackToMenu();
                break;
            case 5:
                ViewListStudentByName();
                BackToMenu();
                break;
            case 6:
                ViewTotalStudent();
                BackToMenu();
                break;
            case 7:
                ViewPercentMaleStudent();
                BackToMenu();
                break;
            case 8:
                ViewPercentFemaleStudent();
                BackToMenu();
                break;
            case 9:
                break;
            default:
                Console.WriteLine("Thank for use program");
                break;
        }
    }
    catch (Exception e)
    {
        Console.WriteLine($"Error: {e.Message}");
    }
} while (choice != AMOUNT_OPTION);

void AddNewStudent()
{
    Message("Add New Student Feature");
    Console.WriteLine("Enter student's name: ");
    string name = Console.ReadLine()!;
    Console.WriteLine("Enter student's age: ");
    int age = int.Parse(Console.ReadLine()!);
    Console.WriteLine("Enter student's gender");
    string gender = Console.ReadLine()!;
    Console.WriteLine("Enter student's major");
    string major = Console.ReadLine()!;
    Student addStudent = new Student(name, age, gender, major);
    mamagement.AddNewStudent(addStudent);
}

void ViewAllStudent()
{
    Message("View All Student Feature");
    mamagement.ViewStudent();
}

void UpdateInformationStudent()
{
    Message("Update Information Student Feature");
    Console.WriteLine("Enter student's id to update: ");
    int updateId = int.Parse(Console.ReadLine()!);
    Console.Clear();
    var oldStudent = mamagement.FindStudentById(updateId);
    if (oldStudent is not null)
    {
        Message("Input Information Student Feature");
        Console.WriteLine("Enter new student's name: ");
        string newName = Console.ReadLine()!;
        Console.WriteLine("Enter new student's age: ");
        int newAge = int.Parse(Console.ReadLine()!);
        Console.WriteLine("Enter new student's gender: ");
        string newGender = Console.ReadLine()!;
        Console.WriteLine("Enter new student's major: ");
        string newMajor = Console.ReadLine()!;
        var updateStudent = new Student(newName, newAge, newGender, newMajor);
        mamagement.UpdateInformationStudent(oldStudent, updateStudent);
    }
}

void RemoveStudent()
{
    Message("Remove Student Feature");
    Console.WriteLine("Enter student's id to remove: ");
    int removeId = int.Parse(Console.ReadLine()!);
    var removeStudent = mamagement.FindStudentById(removeId);
    mamagement.RemoveStudent(removeStudent);
}

void ViewListStudentByName()
{
    Message("View List Student Feature");
    Console.WriteLine("Enter student's name: ");
    string findName = Console.ReadLine()!;
    Console.Clear();
    Message("List Information");
    var studentByName = mamagement.FindStudentByName(findName);
    mamagement.ViewStudentByName(studentByName);
}

void ViewTotalStudent()
{
    Message("View Total Student Feature");
    var totalStudent = mamagement.ViewTotalOfAmountStudent();
    Console.WriteLine($"Total Student is {totalStudent}");
}

void ViewPercentMaleStudent()
{
    Message("View Percent Male Student Feature");
    var percentOfMaleStudent = mamagement.ViewPercentOfMaleStudent();
    Console.WriteLine($"Percent Of Male Student is {percentOfMaleStudent}%");
}

void ViewPercentFemaleStudent()
{
    Message("View Percent Female Student Feature");
    var percentOfFeMaleStudent = mamagement.ViewPercentOfFeMaleStudent();
    Console.WriteLine($"Percent Of Female Student is {percentOfFeMaleStudent}%");
}

void MainMenu()
{
    Message("STUDENT MANAGEMENT PROGRAMING");
    Console.WriteLine("1| Add new student                    *");
    Console.WriteLine("2| View all student                   *");
    Console.WriteLine("3| Update information Student         *");
    Console.WriteLine("4| Remove student                     *");
    Console.WriteLine("5| View list student by name          *");
    Console.WriteLine("6| View total amount of student       *");
    Console.WriteLine("7| View percent male of student       *");
    Console.WriteLine("8| View percent female of student     *");
    Console.WriteLine("9| Exit                               *");
    Console.WriteLine("**************************************");
}

int GetChoice()
{
    try
    {
        Console.Write("Enter your choice: ");
        var choice = int.Parse(Console.ReadLine()!);

        return choice;
    }
    catch (Exception e)
    {
        Console.WriteLine($"Error: {e.Message}");
        return GetChoice();
    }
}

void Message(string message)
{
    Console.WriteLine("**************************************");
    Console.WriteLine($"{message}");
    Console.WriteLine("**************************************");
}

void BackToMenu()
{
    Message("Enter key to back to Menu");
    Console.ReadKey();
    Console.Clear();
}